#ifndef __HARDWARE_H__
#define __HARDWARE_H__

#define VALUE_UPDATE_FREQ_MS		250

/* buttons */
#define BTN1				21
#define BTN2				22
#define BTN3				23

/* encoder */
#define ENC_A				2
#define ENC_B				15
#define ENC_BTN				19

/* stepper */
#define STP_CLK_PIN			17
#define STP_DIR_PIN			13
#define STP_ENA_PIN			16

#define BUZZER_PIN			4

/* LCD */
#define LCD_EN_PIN			14
#define LCD_RS_PIN			12
#define LCD_D4_PIN			27
#define LCD_D5_PIN			26
#define LCD_D6_PIN			25
#define LCD_D7_PIN			33
#define LCD_BL_PIN			32
#define BUZZER_PIN			4
#define LCD_QUEUE_SIZE			24
#define MAX_MESSAGE_SIZE		36
#define LCD_ROW_LENGHT			16

#endif /* __HARDWARE_H__ */
