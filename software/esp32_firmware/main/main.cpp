#include "free_rtos_h.h"
#include "menu.h"
#include "hardware.h"
#include "wifi.h"
#include "ota.h"
#include "driver/gpio.h"
#include "esp_ota_ops.h"
#include "log.h"

extern "C" {
	void app_main();
}

static void gpio_ota_workaround(void)
{
	/* IO2 workaround */
	gpio_reset_pin(GPIO_NUM_2);
	gpio_set_direction(GPIO_NUM_2, GPIO_MODE_OUTPUT);
	gpio_set_level(GPIO_NUM_2, 0);
}

static ota_t ota = {
	.server_ip = "192.168.0.108",
	.server_port = 5007,
	.serial_number = 1,
	.check_interval_ms = 5000,
	.message_size = 0,
	.uniq_magic_word = 0xDEADBEEF,
	.version = 0, /* fetch from app binary */
	.gpio_ota_workaround = gpio_ota_workaround,
	.gpio_ota_cancel_workaround = 0,
};

static void fw_updater(void *args)
{
	delay_s(10);

	INFO("Firmware is valid, confirm image");
	ota_confirm();
	vTaskDelete(NULL);
}

void start_fw_updater(void)
{
	static bool is_running = false;

	if (is_running)
		return;

	is_running = true;
	wifi_init("Tower", "555666777");
	ota_start(&ota);
	INFO("Software updater started...");
}

void app_main(void)
{
	const esp_app_desc_t *app_desc = esp_app_get_description();
	ota.version = app_desc->version;

	xTaskCreate(fw_updater, "updater", 0x1000, 0, 1, 0);
	INFO("Firmware version: %s", ota.version);

	extern void start(const char *version);
	start(ota.version);

	vTaskDelete(NULL);
}

