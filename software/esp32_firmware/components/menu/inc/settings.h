#ifndef __SETTINGS_H__
#define __SETTINGS_H__

#include <stdint.h>

enum motor_torgue {
	MOTOR_TORQUE_100		= 0,
	MOTOR_TORQUE_75			= 1,
	MOTOR_TORQUE_50			= 2,
	MOTOR_TORQUE_20			= 3,
};

typedef struct {
	const char *name;
	uint32_t speed;
	uint32_t acc;
	uint32_t dec;
	uint32_t backlash;
	uint32_t rot_steps;
	uint32_t gear_mult;
	uint32_t gear_mult_dev;
	int32_t correction_stps;
	enum motor_torgue torque;
	int dir_pin;
	int clk_pin;
	int ena_pin;
} motor_settings_t;

void do_settings(void *param);

motor_settings_t *get_current_motor(void);

void copy_settings(motor_settings_t *dst);

#endif /* __SETTINGS_H__ */
