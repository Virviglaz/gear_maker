#ifndef __MENU_H__
#define __MENU_H__

#include <stdint.h>
#include <vector>
#include <string>
#include <functional>
#include "lcd.h"
#include "step_motor.h"

class Menu
{
protected:
	std::string _title;
	std::vector<Menu *> _children = std::vector<Menu *>();
	Menu *parent = nullptr;
	int child_n = 0;

public:
	Menu() {}

	Menu(std::string title,
	     std::vector<Menu *> children = std::vector<Menu *>()) :
	     _title(title),
	     _children(children) {}

	void set_parent(Menu *new_parent) {
		parent = new_parent;
	}

	virtual void init() {}

	virtual void next() {
		if (_children.empty())
			return;
		child_n++;
		if (child_n == _children.capacity())
			child_n = 0;
	}

	virtual void prev() {
		if (_children.empty())
			return;
		if (child_n == 0)
			child_n = _children.capacity() - 1;
		else
			child_n--;
	}

	virtual Menu *enter() {
		if (_children.empty())
			return this;

		auto n = _children[child_n];
		n->set_parent(this);
		return n;
	}

	virtual Menu *back() {
		return parent ? parent : this;
	}

	virtual void update_lcd(Lcd& lcd) {
		lcd.clear();
		lcd.print(Lcd::FIRST_ROW,  Lcd::CENTER, "%s", _title.c_str());
		if (!_children.empty()) {
			auto n = _children[child_n];
			lcd.print(Lcd::SECOND_ROW,
				Lcd::CENTER, "%s", n->_title.c_str());
		}

		lcd.beep();
	}

	virtual void set_value(uint32_t new_val) {}

	virtual uint32_t get_value() {
		return 0;
	}

	virtual uint32_t update_mult() {
		return 0;
	}
};

class MenuSetting : public Menu
{
private:
	uint32_t *_value;
	uint32_t _mult = 1;
	uint32_t _max_mult;
	Menu *_enter;
public:
	MenuSetting() {}

	MenuSetting(std::string title,
		    uint32_t *value,
		    uint32_t max_mult = 1000,
		    Menu *enter = nullptr) {
		_title = title;
		_value = value;
		_max_mult = max_mult;
		_enter = enter;
	}

	void init() {
		_mult = 1;
	}

	Menu *enter() {
		if (_enter) {
			_enter->set_parent(this);
			return _enter;
		}
		return this;
	}

	void update_lcd(Lcd& lcd) {
		lcd.clear();
		lcd.print(Lcd::FIRST_ROW,  Lcd::CENTER, "%s", _title.c_str());
		lcd.print(Lcd::SECOND_ROW, Lcd::LEFT, "x%lu", _mult);
		lcd.print(Lcd::SECOND_ROW, Lcd::CENTER, "%lu", *_value);
	}

	void set_value(uint32_t new_val) {
		*_value = new_val;
	}

	uint32_t get_value() {
		return *_value;
	}

	uint32_t update_mult() {
		_mult *= 10;
		if (_mult >= _max_mult)
			_mult = 1;
		return _mult;
	}
};

class MenuGearCutting : public Menu
{
private:
	uint32_t *_teeth;
	uint32_t tooth = 1;
	Step_motor *_motor;

	uint32_t calc_steps() {
		return 200 * 3 * 40 / *_teeth;
	}

	void motor_run(bool dir) {
		_motor->add_segment(dir ? calc_steps() : -calc_steps(), 1000);
		_motor->run();
		_motor->wait();
	}
public:
	MenuGearCutting() {}

	MenuGearCutting(std::string title,
			uint32_t *teeth,
			Step_motor *motor) {
		_title = title;
		_teeth = teeth;
		_motor = motor;
	}

	void init() {
		tooth = 1;
		if (*_teeth == 0)
			*_teeth = 32;
	}

	void next() {
		tooth++;
		if (tooth == *_teeth)
			tooth = 1;
		motor_run(true);
	}

	void prev() {
		tooth--;
		if (tooth == 0)
			tooth = *_teeth - 1;
		motor_run(false);
	}

	Menu *back() {
		_motor->disable();
		return Menu::back();
	}

	void update_lcd(Lcd& lcd) {
		lcd.clear();
		lcd.print(Lcd::FIRST_ROW,  Lcd::CENTER, "%s", _title.c_str());
		lcd.print(Lcd::SECOND_ROW, Lcd::CENTER, "%lu", tooth);
		lcd.beep();
	}
};

class MenuRotating : public Menu
{
private:
	Step_motor *_motor;
	uint32_t _speed;
	bool _dir;
	bool is_running = false;
public:
	MenuRotating() {}

	MenuRotating(std::string title,
		     Step_motor *motor,
		     uint32_t speed,
		     bool dir = false) {
		_title = title;
		_motor = motor;
		_speed = speed;
		_dir = dir;
	}

	void update_lcd(Lcd& lcd) {
		lcd.clear();
		lcd.print(Lcd::FIRST_ROW,  Lcd::CENTER, "%s",
			is_running ? "RUNNING" : "ON HOLD");
		lcd.print(Lcd::SECOND_ROW, Lcd::CENTER, "%s",
			_dir ? "CWW" : "CW");
		if (is_running) {
			_motor->add_segment(
				_dir ? -INT32_MAX : INT32_MAX, _speed);
			_motor->run();
		} else {
			_motor->stop();
		}
		is_running = !is_running;
	}

	Menu *back() {
		_motor->stop();
		_motor->disable();
		return Menu::back();
	}

};

class MenuExe : public Menu
{
private:
	std::function<void()> _handler;
public:
	MenuExe() {}

	MenuExe(std::string title, std::function<void()> handler) {
		_title = title;
		_handler = handler;
	}

	void init() {
		_handler();
	}

};

#endif /* __MENU_H__ */
