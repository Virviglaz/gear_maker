#include <free_rtos_h.h>
#include <esp_buttons.h>
#include <esp_encoder.h>
#include <esp_stepper.h>
#include "nvm.h"
#include "menu.h"

#include "step_motor.h"

static motor_settings_t cur_motor;
typedef std::vector<Menu *> item_list;
Step_motor motor(STP_ENA_PIN, STP_CLK_PIN, STP_DIR_PIN, 1000);

static uint32_t index_n = 32;
static MenuGearCutting gear_cutting("TOOTH", &index_n, &motor);
static MenuSetting indexing("INDEXING", &index_n, 1, &gear_cutting);

static MenuSetting set_speed("SPEED", &cur_motor.speed);
static MenuSetting set_acc("ACCELERATION", &cur_motor.acc);
static MenuSetting set_dec("DECELERATION", &cur_motor.dec);
static MenuSetting set_backlash("BACKLASH", &cur_motor.backlash);
static Menu setting("SETTINGS", item_list {
	&set_speed,
	&set_acc,
	&set_dec,
	&set_backlash });

static MenuRotating run_slow_cw("SLOW CW", &motor, 250, false);
static MenuRotating run_slow_ccw("SLOW CCW", &motor, 250, true);
static MenuRotating run_mid_cw("MEDIUM CW", &motor, 500, false);
static MenuRotating run_mid_ccw("MEDIUM CCW", &motor, 500, true);
static MenuRotating run_fast_cw("FAST CW", &motor, 1000, false);
static MenuRotating run_fast_ccw("FAST CCW", &motor, 1000, true);

static Menu free_run("FREE RUNNING", item_list {
	&run_slow_cw,
	&run_slow_ccw,
	&run_mid_cw,
	&run_mid_ccw,
	&run_fast_cw,
	&run_fast_ccw });

extern void start_fw_updater(void);
static MenuExe fw_upd("RUN FW UPDATE", start_fw_updater);

static Menu top("MENU", item_list {
	&indexing,
	&setting,
	&free_run,
	&fw_upd });

void start(const char *version)
{
	Lcd lcd;
	Buttons buttons;
	Encoder<int32_t> enc;

	lcd.clear();
	lcd.print(Lcd::FIRST_ROW, Lcd::CENTER, "VERSION");
	lcd.print(Lcd::SECOND_ROW, Lcd::CENTER, "%s", version);

	copy_settings(&cur_motor);
	restore_data(MOTOR_SETTINGS, (void *)&cur_motor);

	delay_s(2);
	lcd.clear();
	motor.init();

	buttons.add(ENC_BTN);		/* 0 */
	buttons.add(BTN1);		/* 1 */
	buttons.add(BTN2);		/* 2 */
	buttons.add(BTN3);		/* 3 */
	buttons.add(ENC_A);		/* 4 */
	buttons.add(ENC_B);		/* 5 */
	enc.init(ENC_B, ENC_A);

	Menu *m = &top;

	while (1) {
		m->update_lcd(lcd);

		int pressed = buttons.wait_for_action();
		m->set_value(enc.get_value());

		switch (pressed) {
		case 0:
			m = m->enter();
			m->init();
			if (m->update_mult())
				enc.set_step(m->update_mult());
			enc.set_value(m->get_value());
			break;
		case 1:
			m->prev();
			break;
		case 2:
			m->next();
			break;
		case 3:
			m = m->back();
			break;
		default:
			break;
		}
	}
}
