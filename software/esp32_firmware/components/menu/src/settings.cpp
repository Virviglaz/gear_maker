#include "settings.h"
#include "esp_buttons.h"
#include "esp_encoder.h"
#include "free_rtos_h.h"
#include "menu.h"
#include "hardware.h"
#include "nvm.h"
#include "generic.h"

static SemaphoreHandle_t wait;

typedef struct {
	const char *name;		/* item name */
	const uint32_t max, min;	/* limits */
	const uint8_t mult_n;		/* max multiplier */
	uint32_t *value;		/* pointer to value */
	const char *units;		/* uints of value */
	const char *format;
} settings_t;

/* default settings */
#if defined(M42_GEARED512_MOTOR)
static motor_settings_t m42_gear512_motor = {
	.name = "Hanpose 17HS8401",
	.speed = 800,
	.acc = 3200,
	.dec = 3200,
	.backlash = 400,
	.rot_steps = 200,
	.gear_mult = 518 * 40,
	.gear_mult_dev = 100, /* gear_mult / gear_mult_dev: 5.18 x 100 = 518 */
	.correction_stps = 100, /* additional steps for correction */
	.torque = MOTOR_TORQUE_100,
	.dir_pin = STP_DIR_PIN,
	.clk_pin = STP_CLK_PIN,
	.ena_pin = STP_ENA_PIN,
};
static motor_settings_t *cur_motor = &m42_gear512_motor;
#else
	static motor_settings_t *cur_motor = 0;
	#error "Please, specify the default motor settings"
#endif

static const settings_t settings[] = {
	/* actual running speed in steps per second */
	{
		.name = "SPEED",
		.max = 3200 * 20,
		.min = 320,
		.mult_n = 4,
		.value = &cur_motor->speed,
		.units = "SPS",
		.format = "%s: %5.5u %s",
	},
	/* acceleration in steps per second^2 */
	{
		.name = "ACC",
		.max = 3200 * 50,
		.min = 320,
		.mult_n = 4,
		.value = &cur_motor->acc,
		.units = "SPSS",
		.format = "%s: %5.5u %s",
	},
	/* deceleration in steps per second^2 */
	{
		.name = "DEC",
		.max = 3200 * 50,
		.min = 320,
		.mult_n = 4,
		.value = &cur_motor->dec,
		.units = "SPSS",
		.format = "%s: %5.5u %s",
	},
	/* backlash distance in steps */
	{
		.name = "BACKLASH",
		.max = 9999,
		.min = 0,
		.mult_n = 3,
		.value = &cur_motor->backlash,
		.units = "S",
		.format = "%s: %4.4u %s",
	},
	/* full motor rotate in steps. Normally 3200 if microstep == 16 */
	{
		.name = "ROT STPS",
		.max = 9999,
		.min = 100,
		.mult_n = 3,
		.value = &cur_motor->rot_steps,
		.units = "S",
		.format = "%s: %4.4u %s",
	},
	/* addition gear ration if geared motor is used multiplied by 10 */
	{
		.name = "GEAR Rx10",
		.max = 1,
		.min = 100,
		.mult_n = 2,
		.value = &cur_motor->gear_mult,
		.units = "",
		.format = "%s: %4.4u %s",
	},
};

static uint8_t select_job(const settings_t *item, uint8_t item_n, bool *done)
{
	//uint8_t mult_n = 0;
	//uint32_t prev_value = 0;
	//uint8_t prev_mult = 0;

	//buttons *btn = new buttons();
	//encoder *enc = new encoder(ENC_A, ENC_B);

	//glcd->clear();
	//glcd->print(SECOND_ROW, RIGHT, "%s", std_btn);

	/*btn->add(BTN1, prev_button, NEGEDGE, &item_n);
	btn->add(BTN2, next_button, NEGEDGE, &item_n);
	btn->add(BTN3, enter_button, NEGEDGE, done);
	btn->add(ENC_BTN, enc_button, NEGEDGE, &mult_n);*/

	//enc->set_limits(item->min, item->max);
	//enc->set_value(*item->value);
	//enc->invert();

	/*while (xSemaphoreTake(wait, VALUE_UPDATE_FREQ_MS) != pdTRUE) {
		*item->value = enc->get_value();
		if (*item->value == prev_value && mult_n == prev_mult)
			continue;

		prev_value = *item->value;
		prev_mult = mult_n;
		if (mult_n == item->mult_n)
			mult_n = 0;

		//glcd->print(FIRST_ROW, CENTER, item->format,
			//item->name, *item->value, item->units);
		const mult_t *mult = get_mult(mult_n);
		//glcd->print(SECOND_ROW, LEFT, "%5.5s", mult->desc);
		//enc->set_step(-mult->mult);
	}*/

	//delete(btn);
	//delete(enc);

	return 0;
	//return item_n;
}

void do_settings(void *param)
{
	wait = (SemaphoreHandle_t)param;
	uint8_t item_n = 0;

	while (1) {
		const settings_t *item = &settings[item_n];
		bool done = false;
		item_n = select_job(item, item_n, &done);

		if (done) {
			store_data(MOTOR_SETTINGS, (void *)cur_motor);
			return;
		}
	}
}

motor_settings_t *get_current_motor(void)
{
	return cur_motor;
}

void copy_settings(motor_settings_t *dst)
{
	memcpy(dst, &cur_motor, sizeof(cur_motor));
}
