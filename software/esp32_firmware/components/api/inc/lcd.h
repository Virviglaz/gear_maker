#ifndef __LCD_H__
#define __LCD_H__

/* FreeRTOS */
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

/* ESP32 Drivers */
#include <driver/gpio.h>
#include <esp_timer.h>

/* LCD Driver */
#include <HD44780.h>

/* Standart types */
#include <stdint.h>
#include <string.h>

/* LCD connection */
#include "hardware.h"

class Lcd {
public:
	enum row_e {
		FIRST_ROW,
		SECOND_ROW
	};

	enum align {
		LEFT,
		CENTER,
		RIGHT,
	};

	Lcd() {
		configure_gpio();
		queue = xQueueCreate(LCD_QUEUE_SIZE, sizeof(msg_t));
		xTaskCreate(handler, "lcd", 0x1000, this, 1, &handle);
	}

	~Lcd() {
		is_active = false;
		clear(); /* send dummy message */
	}

	void print(const char *format, ...) {
		va_list args;
		va_start(args, format);
		msg_t msg = prepare(msg->text, format, args);
		va_end(args);

		if (msg) {
			msg->row = 0;
			msg->col = 0;
			msg->clear = false;
			msg->beep = false;
			xQueueSend(queue, &msg, portMAX_DELAY);
		}
	}

	void print(enum row_e row, uint8_t col, const char *format, ...) {
		va_list args;
		va_start(args, format);
		msg_t msg = prepare(msg->text, format, args);
		va_end(args);

		if (msg) {
			msg->row = (uint8_t)row;
			msg->col = col < LCD_ROW_LENGHT ? col : 0;
			msg->clear = false;
			msg->beep = false;

			xQueueSend(queue, &msg, portMAX_DELAY);
		}
	}

	void print(enum row_e row, enum align a, const char *format, ...) {
		va_list args;
		va_start(args, format);
		msg_t msg = prepare(msg->text, format, args);
		va_end(args);

		if (msg) {
			uint8_t col = 0;

			switch (a) {
			case LEFT:
				break;
			case RIGHT:
				col = LCD_ROW_LENGHT - strlen(msg->text);
				break;
			case CENTER:
				col = LCD_ROW_LENGHT - strlen(msg->text);
				col /= 2;
			}

			msg->row = row < 2 ? row : 1;
			msg->col = col < LCD_ROW_LENGHT ?
				col : LCD_ROW_LENGHT - col;
			msg->clear = false;
			msg->beep = false;

			xQueueSend(queue, &msg, portMAX_DELAY);
		}
	}

	void clear() {
		msg_t msg = (msg_t)malloc(sizeof(*msg));

		if (msg) {
			msg->text[0] = 0;
			msg->row = 0;
			msg->col = 0;
			msg->clear = true;
			msg->beep = false;

			xQueueSend(queue, &msg, portMAX_DELAY);
		}
	}

	void clear(enum row_e row) {
		msg_t msg = (msg_t)malloc(sizeof(*msg));

		if (msg) {
			memset(msg->text, ' ', LCD_ROW_LENGHT);
			msg->text[LCD_ROW_LENGHT] = 0;
			msg->row = row;
			msg->col = 0;
			msg->clear = false;
			msg->beep = false;

			xQueueSend(queue, &msg, portMAX_DELAY);
		}
	}

	void beep() {
		msg_t msg = (msg_t)malloc(sizeof(*msg));

		if (msg) {
			msg->text[0] = 0;
			msg->clear = false;
			msg->beep = true;

			xQueueSend(queue, &msg, portMAX_DELAY);
		}
	}

private:
	typedef struct {
		uint8_t row;
		uint8_t col;
		bool clear;
		bool beep;
		char text[MAX_MESSAGE_SIZE];
	} *msg_t;

	static void write(uint8_t data) {
		gpio_set_level((gpio_num_t)LCD_D4_PIN, data & BIT(0));
		gpio_set_level((gpio_num_t)LCD_D5_PIN, data & BIT(1));
		gpio_set_level((gpio_num_t)LCD_D6_PIN, data & BIT(2));
		gpio_set_level((gpio_num_t)LCD_D7_PIN, data & BIT(3));
		gpio_set_level((gpio_num_t)LCD_RS_PIN, data & BIT(4));
		gpio_set_level((gpio_num_t)LCD_EN_PIN, data & BIT(5));
		gpio_set_level((gpio_num_t)LCD_BL_PIN, data & BIT(6));
	}

	static void delay_func(uint16_t us) {
		int64_t time = esp_timer_get_time() + us;
		while (time > esp_timer_get_time())
			taskYIELD();
	}

	void configure_gpio(void) {
		constexpr struct {
			const char *name;	/* function name */
			int pin;		/* pin number */
		} gpios[] = {
			{ "LCD_EN_PIN",		LCD_EN_PIN },
			{ "LCD_RS_PIN",		LCD_RS_PIN },
			{ "LCD_D4_PIN",		LCD_D4_PIN },
			{ "LCD_D5_PIN",		LCD_D5_PIN },
			{ "LCD_D6_PIN",		LCD_D6_PIN },
			{ "LCD_D7_PIN",		LCD_D7_PIN },
			{ "LCD_BL_PIN",		LCD_BL_PIN },
			{ "BUZZER_PIN",		BUZZER_PIN },
		};

		for (int i = 0; i != sizeof(gpios) / sizeof(gpios[0]); i++) {
			ESP_ERROR_CHECK(
				gpio_reset_pin((gpio_num_t)gpios[i].pin));
			ESP_ERROR_CHECK(
				gpio_set_direction((gpio_num_t)gpios[i].pin,
				GPIO_MODE_OUTPUT));
			ESP_ERROR_CHECK(
				gpio_set_level((gpio_num_t)gpios[i].pin, 0));
		}
	}

	msg_t prepare(char *dest, const char *format, va_list arg) {
		msg_t msg = (msg_t)malloc(sizeof(*msg));
		if (msg)
			vsnprintf(msg->text, MAX_MESSAGE_SIZE, format, arg);
		return msg;
	}

	static void handler(void *arg) {
		struct hd44780_conn con = {
			.bus_type = HD44780_BUS_4B,
			.data_shift = 0,
			.rs_pin = BIT(4),
			.en_pin = BIT(5),
			.backlight_pin = BIT(6),
		};

		struct hd44780_lcd hd44780 = {
			.write = write,
			.write16 = 0,
			.delay_us = delay_func,
			.is_backlight_enabled = 1,
			.type = HD44780_TYPE_LCD,
			.font = HD44780_ENGLISH_RUSSIAN_FONT,
			.conn = &con,
			.ext_con = 0,
		};
		Lcd *l = (Lcd *)arg;

		hd44780_init(&hd44780);

		while (l->is_active) {
			msg_t msg;

			xQueueReceive(l->queue, &msg, portMAX_DELAY);

			if (msg->clear)
				hd44780_clear();

			if (msg->text[0]) {
				hd44780_set_pos(msg->row, msg->col);
				hd44780_print(msg->text);
			}

			if (msg->beep) {
				gpio_set_level((gpio_num_t)BUZZER_PIN, 1);
				delay_ms(50);
				gpio_set_level((gpio_num_t)BUZZER_PIN, 0);
			}

			free(msg);
		}

		vQueueDelete(l->queue);
		vTaskDelete(nullptr);
	}

	QueueHandle_t queue;
	TaskHandle_t handle = NULL;
	bool is_active = true;
};

#endif /* __LCD_H__ */
