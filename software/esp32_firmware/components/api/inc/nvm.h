#ifndef __NVM_H__
#define __NVM_H__

enum store_e {
	MOTOR_SETTINGS,
};

#include "settings.h"

void restore_data(enum store_e type, void *data);
void store_data(enum store_e type, void *data);

#endif /* __NVM_H__ */
