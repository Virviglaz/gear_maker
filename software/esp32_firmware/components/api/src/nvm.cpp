#include "nvm.h"
#include "free_rtos_h.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "log.h"
#include <string.h>

#include "spi_flash_mmap.h"
//#include "esp_spi_flash.h"

#define DATABASE_PARTITION_SIZE			SPI_FLASH_SEC_SIZE
#define MAGIC					0xDEADBEEF

static const char *db_partition_name = "database";

typedef struct {
	uint32_t magic;
	uint32_t size;
	motor_settings_t motor;
} *datastore_t;

static const esp_partition_t *get_partition(void)
{
	const esp_partition_t *p =
		esp_partition_find_first(ESP_PARTITION_TYPE_DATA,
		ESP_PARTITION_SUBTYPE_DATA_NVS, db_partition_name);
		if (!p)
			ERROR("Partition %s not found in flash",
				db_partition_name);
	return p;
}

void restore_data(enum store_e type, void *data)
{
	const esp_partition_t *p = get_partition();
	esp_err_t res;
	if (!p)
		return;

	datastore_t d = (datastore_t)malloc(DATABASE_PARTITION_SIZE);
	if (!d) {
		ERROR("No memory");
		return;
	}

	res = esp_partition_read(p, 0, d, sizeof(*d));
	if (res) {
		ERROR("Reading %s partition failed: %s",
			db_partition_name, esp_err_to_name(res));
		goto done;
	}

	/* check data is valid */
	if (d->magic != MAGIC) {
		WARN("Fail to recover data: magic word is wrong.");
		goto done;
	}

	/* check store version */
	if (d->size != sizeof(*d)) {
		WARN("Fail to recover data: size mismatch: " \
			"expected %u, got: %lu", sizeof(*d), d->size);
		goto done;
	}

	switch (type)
	{
	case MOTOR_SETTINGS:
		memcpy(data, &d->motor, sizeof(d->motor));
		INFO("Data recovered.");
		break;
	default:
		ERROR("Undefined condition");
		break;
	}
	
done:
	free(d);
}

void store_data(enum store_e type, void *data)
{
	const esp_partition_t *p = get_partition();
	esp_err_t res;
	if (!p)
		return;

	datastore_t d = (datastore_t)malloc(DATABASE_PARTITION_SIZE);
	if (!d) {
		ERROR("No memory");
		return;
	}

	res = esp_partition_read(p, 0, d, sizeof(*d));
	if (res) {
		ERROR("Reading %s partition failed: %s",
			db_partition_name, esp_err_to_name(res));
		goto done;
	}

	d->magic = MAGIC;
	d->size = sizeof(*d);

	switch (type)
	{
	case MOTOR_SETTINGS:
		if (!memcmp(&d->motor, data, sizeof(d->motor)))
			goto done;
		memcpy(&d->motor, data, sizeof(d->motor));
		break;
	default:
		ERROR("Undefined condition");
		goto done;
	}

	res = esp_partition_erase_range(p, 0, DATABASE_PARTITION_SIZE);
	if (res)
		ERROR("Erasing database partition failed: %s",
			esp_err_to_name(res));
	
	res = esp_partition_write(p, 0, d, DATABASE_PARTITION_SIZE);
	if (res)
		ERROR("Writing database partition failed: %s",
			esp_err_to_name(res));

	INFO("Saved %lu bytes of data.", d->size);	
done:
	free(d);
}
