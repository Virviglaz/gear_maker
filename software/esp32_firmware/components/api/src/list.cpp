/*
 * This file is provided under a MIT license.  When using or
 * redistributing this file, you may do so under either license.
 *
 * MIT License
 *
 * Copyright (c) 2022 Pavel Nadein
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Linked list
 *
 * Contact Information:
 * Pavel Nadein <pavelnadein@gmail.com>
 */

#include "list.h"
#include <errno.h>

class node
{
public:
	node(void *e, int i) : entry(e), index(i), next(0) {}

	void *entry;
	int index;
	node *next;
};

/* PUBLIC */
int list::add(void *entry)
{
	node *add_node = new node(entry, index++);
	if (!add_node) {
		if (error)
			error(ENOMEM);
		return -ENOMEM;
	}

	node *top = get_last();
	if (!top)
		first = add_node;
	else
		top->next = add_node;

	return index;
}

void *list::get(int index)
{
	node *node = first;
	if (!first)
		return 0;

	while (node->next) {
		if (node->index == index)
			return node->entry;
		node = node->next;
	}

	return 0;
}

int list::get(void *entry)
{
	node *node = first;
	if (!first)
		return -EINVAL;

	while (node->next) {
		if (node->entry == entry)
			return node->index;
		node = node->next;
	}

	return -EINVAL;
}

int list::remove(int index)
{
	node *node = get_last();
	if (!node)
		return -EINVAL;
	
	if (node == first) {
		delete(first);
		return 0;
	}

	//node *prev = node;
	while (node->next) {
		//prev = node;

		if (node->index == index) {
			delete(node->next);
			node->next = 0;
		}
			
		node = node->next;
	}
	return 0;
}

/* PRIVATE */
node *list::get_last()
{
	node *node = first;
	if (!first)
		return 0;

	while (node->next)
		node = node->next;

	return node;
}

